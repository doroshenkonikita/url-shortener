class Links < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :default_url
      t.string :shortened_key
      t.integer      :user_id
    end
  end
end
