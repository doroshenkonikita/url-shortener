require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/flash'
require 'require_all'
require 'config'

require_all 'lib/models'
require_all 'lib/concerns'

LABEL_KEY = /[0-9A-Za-z_]+/.freeze

class App < Sinatra::Base
  register Sinatra::ActiveRecordExtension
  register Sinatra::Flash

  register Config
  Config.load_and_set_settings("config/settings.yml")

  enable :sessions
  set :session_secret, Settings.session_secret

  before do
    @user = User.find_or_create_by(session_id: session[:session_id].to_s)
  end

  get '/' do
    links = Link.where(user_id: @user.id)
    erb :index, locals: { links: links }
  end

  post '/' do
    default_url = params[:url]
    link = Link.create(default_url: default_url, user_id: @user.id)
    redirect '/'
  end

  get %r{/(#{ShortenKey::SHORTEN_KEY_REGEX}|#{LABEL_KEY})} do
    received_key = request.path_info.delete '/'
    link = Link.find_by(shortened_key: received_key)
    status 404 && not_found unless link
    redirect link.default_url
  end

  get %r{/(#{ShortenKey::SHORTEN_KEY_REGEX}|#{LABEL_KEY})/edit} do
    received_key = request.path_info.gsub('/', '').gsub('edit', '')
    link = Link.find_by(shortened_key: received_key)

    status 404 && not_found unless link
    status 404 && not_found unless @user.id == link.user_id

    erb :edit, locals: { link: link}
  end

  post %r{/(#{ShortenKey::SHORTEN_KEY_REGEX}|#{LABEL_KEY})/edit} do
    received_key = request.path_info.gsub('/', '').gsub('edit', '')
    link = Link.find_by(shortened_key: received_key)
    new_label = params[:label]

    status 404 && not_found unless link
    status 404 && not_found unless @user.id == link.user_id

    unless new_label =~ LABEL_KEY
      flash[:error] = "Label can containt only numbers, letters and _"
      redirect "/#{link.shortened_key}/edit"
    end

    link.update(shortened_key: new_label)

    redirect '/'
  end

  not_found do
    erb :not_found
  end

end