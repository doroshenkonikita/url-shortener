class ShortenKey

  SHORTEN_KEY_LENGTH = Settings.shorten_key_length.freeze
  SHORTEN_KEY_REGEX = /[0-9a-zA-Z]+/.freeze

  class << self
    def generate_key
      atoms = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
      (0...SHORTEN_KEY_LENGTH).map { atoms[rand(atoms.length)] }.join
    end
  end
end