Config.load_and_set_settings("config/settings.yml")

class Link < ActiveRecord::Base
  before_create :setup_shortened_key

  def shortened_url
    "#{Settings.production_url}/#{shortened_key}"
  end

  private

    def setup_shortened_key
      self.shortened_key = shorten
    end

    def shorten
      key = ShortenKey.generate_key
      shorten if Link.where(shortened_key: key).any?
      key
    end

end